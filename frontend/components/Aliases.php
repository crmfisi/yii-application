<?php
namespace frontend\components;

use Yii;
use yii\base\Component;

class Aliases extends Component {

    public $assetUrl;

    public function init() {
        parent::init();

        $this->assetUrl = Yii::$app->assetManager->getPublishedUrl('@frontend/themes/cuponsmart/assets');
    }

    public function getAssetsUrl() {
        return $this->assetUrl;
    }

}