<?php
/**
 * User created: Cesar Mamani Dominguez
 * Date: 27/01/2016
 * Time: 05:04 PM
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\JsAsset;
use common\widgets\Alert;
/* @var $this \yii\web\View */
/* @var $content string */
AppAsset::register($this);
JsAsset::register($this);
?>
<?php $this->beginPage() ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="description" content="">

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php echo $this->renderFile('@frontend/themes/cuponsmart/views/layouts/header.php'); ?>
<?php echo $content; ?>
<?php echo $this->renderFile('@frontend/themes/cuponsmart/views/layouts/footer.php'); ?>


<?php $this->endBody() ?>

<?php $this->endPage() ?>