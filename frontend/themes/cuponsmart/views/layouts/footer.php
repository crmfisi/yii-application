<?php
/**
 * User created: Cesar Mamani Dominguez
 * Date: 27/01/2016
 * Time: 05:04 PM
 */
?>

<footer>
    <div class="container">
        <i class="fa fa-cogs"></i>

        <div class="row">

            <div class="col-sm-6 links">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Información</h3>
                        <ul class="list-unstyled">
                            <li><a href="">Nosotros</a></li>
                            <li><a href="">Políticas de Privacidad</a></li>
                            <li><a href="">Términos y condiciones</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Nuestro Servicio</h3>
                        <ul class="list-unstyled">
                            <li><a href="">Contacto</a></li>
                            <li><a href="">Devoluciones</a></li>
                            <li><a href="">Sitemap</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Cupon Smart</h3>
                        <ul class="list-unstyled">
                            <li><a href="">Como funciona</a></li>
                            <li><a href="">Quienes somos</a></li>
                            <li><a href="">Publica con nosotros</a></li>
                            <li><a href="">Atención al usuario</a></li>
                            <li><a href="">Libro de reclamaciones</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Social -->
            <div class="col-sm-6 social">
                <ul class="list-unstyled">
                    <li><a href="" class="facebook">follow @facebook <i class="fa fa-facebook"></i></a></li>
                    <li><a href="" class="twitter">follow @twitter <i class="fa fa-twitter"></i></a></li>
                    <li><a href="" class="youtube">follow @youtube channel <i class="fa fa-youtube"></i></a></li>
                    <li><a href="skype:********?call" class="skype">skype call <i class="fa fa-skype"></i></a></li>
                </ul>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="powered">Diseñado por <a href="">Catarxis Technologies</a> © 2015</div>
            </div>
        </div>

    </div>

</footer>

<div id="back-top" style="display: none;"><a href=""><i class="fa fa-angle-up"></i></a></div>

<script>
    $(document).ready(function() {

        $(".slideshow").owlCarousel({
            items: 6,
            autoPlay: 6000,
            singleItem: true,
            navigation: true,
            responsive: true,
            navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            pagination: true
        });

        $(".carousel").owlCarousel({
            items: 6,
            autoPlay: 3000,
            navigation: true,
            navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            pagination: true
        });

        $("#back-top").hide();

        $("#back-top a").click(function(e) {
            e.preventDefault();
            $("body, html").animate({
                scrollTop: 0
            }, 300);
        });

        $(window).scroll(function() {

            var nav = $("#sticky");

            if ($(this).scrollTop() > 145) {
                nav.addClass("fixed-header");
            } else {
                nav.removeClass("fixed-header");
            }

            if ($(this).scrollTop() > 100) {
                $("#back-top").fadeIn();
            } else {
                $("#back-top").fadeOut();
            }
        });

    });
</script>

</body>
</html>
