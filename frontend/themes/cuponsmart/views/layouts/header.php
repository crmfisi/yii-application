<?php
/**
 * User created: Cesar Mamani Dominguez
 * Date: 27/01/2016
 * Time: 05:04 PM
 */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Ads;
use common\models\Category;
use common\models\Subcategory;

/* @var $this \yii\web\View */
/* @var $content string */

$categories = Category::find()->all();
$AssetUrl = Yii::$app->frontend_alias->getAssetsUrl();
?>
<nav class="top-nav">
    <div class="container">

        <!-- Links -->
        <div class="links-inline pull-right">
            <ul class="list-inline">
                <li class="dropdown">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user hidden-sm hidden-md hidden-lg"></i>
                        <span class="hidden-xs">Mi Cuenta</span>
                    </a>
                    <i class="fa fa-angle-down"></i>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown-triangle"></li>
                        <li><a href="">Registro</a></li>
                        <li><a href="">Ingresar</a></li>
                    </ul>
                </li>
                <li><a href="">
                        <i class="fa fa-heart hidden-sm hidden-md hidden-lg"></i>
                        <span class="hidden-xs">Lista Deseados (0)</span>
                    </a></li>
                <li><a href="">
                        <i class="fa fa-shopping-cart hidden-sm hidden-md hidden-lg"></i>
                        <span class="hidden-xs">Carrito</span>
                    </a></li>
                <!-- <li><a href="">
                  <i class="fa fa-share hidden-sm hidden-md hidden-lg"></i>
                  <span class="hidden-xs">Finalizar Compra</span>
                </a></li> -->
            </ul>
        </div>

    </div>
</nav>

<div id="sticky">

    <header>
        <div class="container">
            <div class="row">

                <!-- Logo -->
                <div id="logo" class="col-sm-3">
                    <a href="<?php echo Url::toRoute('/'); ?>"><img src="<?php echo $AssetUrl; ?>/img/logo.png"
                                                                    title="Logo" alt="Logo"></a>
                </div>

                <!-- Menu -->
                <div class="col-sm-7">
                    <nav id="menu" class="navbar">
                        <!--<div class="navbar-header">
                            <span id="category" class="visible-xs">Categories</span>
                            <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-ex1-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>-->
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="<?php echo Url::toRoute('/ads'); ?>">Destacados</a>
                                </li>
                                <?php foreach ($categories as $category) {
                                    $sb = Subcategory::find()->where(['category_id' => (string)$category->_id])->all();
                                    if (count($sb) > 0) { ?>
                                        <li class="dropdown">
                                            <a href="" class="dropdown-toggle" data-toggle="dropdown"
                                               role="button"><?php echo $category->name ?>
                                                <i class="fa fa-angle-down"></i></a>
                                            <span class="dropdown-triangle"></span>

                                            <div class="dropdown-menu">
                                                <div class="dropdown-inner">
                                                    <ul class="list-unstyled">
                                                        <?php $subcategories = Subcategory::find()->where(['category_id' => (string)$category->_id])->all();
                                                        // var_dump($subcategories);
                                                        foreach ($subcategories as $subcategory) {
                                                            $ads = Ads::find()->where(['subcategory_id' => (string)$subcategory->_id])->all();
                                                            ?>
                                                            <li>
                                                                <?php echo Html::a(Html::encode($subcategory->name . ' (' . count($ads) . ')'),
                                                                    [
                                                                        'filter_subcategory',
                                                                        'subcategory' => (string)$subcategory->_id
                                                                    ]) ?>
                                                                <!--<a href="grid.html"><?php /*echo $subcategory->name */ ?></a>-->
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </nav>
                </div>

                <!-- Shopping Cart -->
                <div class="col-sm-2">
                    <div class="shopping-cart">
                        <button type="button" data-toggle="dropdown"
                                class="btn btn-inverse btn-lg btn-block dropdown-toggle">
                            <span class="cart-text">S/.0.00</span>
                  <span class="cart-icon"><strong>0</strong>
                    <span class="cart-icon-handle"></span>
                  </span>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </header>
</div>