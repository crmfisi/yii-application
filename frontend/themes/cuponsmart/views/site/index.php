<?php
/**
 * User created: Cesar Mamani Dominguez
 * Date: 27/01/2016
 * Time: 05:05 PM
 */
$this->title = 'Cuponsmart :: Cupones para toda ocasión';
$AssetUrl =  Yii::$app->frontend_alias->getAssetsUrl();
?>

<div class="container">

    <!-- Slideshow -->
    <div class="row">
        <div class="col-sm-12">
            <div id="owl-example" class="slideshow">
                <div><img src="<?php echo $AssetUrl; ?>/img/banner_mn_1.jpg" alt=""></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/banner_mn_2.jpg" alt=""></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/banner_mn_3.jpg" alt=""></div>
            </div>
        </div>
    </div>

    <h3>Últimas Ofertas</h3>
    <div class="row">

        <!-- Producto -->
        <div class="product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-item">
                <div class="item-overlay">
                    <div class="clickable">
                        <a href="">Mochila para Laptop</a>
                    </div>
                </div>
                <div class="image">
                    <a href=""><img src="<?php echo $AssetUrl; ?>/img/product_x800_1.jpg" alt="Product 1"></a>
                </div>
                <div class="caption">
                    <div class="name">
                        <a href="">Mochila para Laptop</a>
                    </div>
                    <div class="price">
                        <span class="price-new">S./ 120.00</span>
                    </div>
                    <div class="cart">
                        <button type="button" class="btn btn-primary">Ver Detalle</button>
                    </div>
                </div>
                <button type="button" class="btn btn-default wishlist" data-toggle="tooltip" data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                <button type="button" class="btn btn-default compare" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
            </div>
        </div>

        <!-- Producto -->
        <div class="product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-item">
                <div class="item-overlay">
                    <div class="clickable">
                        <a href="">Mochila para Laptop</a>
                    </div>
                </div>
                <div class="image">
                    <a href=""><img src="<?php echo $AssetUrl; ?>/img/product_x800_1.jpg" alt="Product 1"></a>
                </div>
                <div class="caption">
                    <div class="name">
                        <a href="">Mochila para Laptop</a>
                    </div>
                    <div class="price">
                        <span class="price-new">S./ 120.00</span>
                    </div>
                    <div class="cart">
                        <button type="button" class="btn btn-primary">Ver Detalle</button>
                    </div>
                </div>
                <button type="button" class="btn btn-default wishlist" data-toggle="tooltip" data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                <button type="button" class="btn btn-default compare" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
            </div>
        </div>

        <!-- Producto -->
        <div class="product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-item">
                <div class="item-overlay">
                    <div class="clickable">
                        <a href="">Mochila para Laptop</a>
                    </div>
                </div>
                <div class="image">
                    <a href=""><img src="<?php echo $AssetUrl; ?>/img/product_x800_1.jpg" alt="Product 1"></a>
                </div>
                <div class="caption">
                    <div class="name">
                        <a href="">Mochila para Laptop</a>
                    </div>
                    <div class="price">
                        <span class="price-new">S./ 120.00</span>
                    </div>
                    <div class="cart">
                        <button type="button" class="btn btn-primary">Ver Detalle</button>
                    </div>
                </div>
                <button type="button" class="btn btn-default wishlist" data-toggle="tooltip" data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                <button type="button" class="btn btn-default compare" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
            </div>
        </div>

        <!-- Producto -->
        <div class="product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-item">
                <div class="item-overlay">
                    <div class="clickable">
                        <a href="">Mochila para Laptop</a>
                    </div>
                </div>
                <div class="image">
                    <a href=""><img src="<?php echo $AssetUrl; ?>/img/product_x800_1.jpg" alt="Product 1"></a>
                </div>
                <div class="caption">
                    <div class="name">
                        <a href="">Mochila para Laptop</a>
                    </div>
                    <div class="price">
                        <span class="price-new">S./ 120.00</span>
                    </div>
                    <div class="cart">
                        <button type="button" class="btn btn-primary">Ver Detalle</button>
                    </div>
                </div>
                <button type="button" class="btn btn-default wishlist" data-toggle="tooltip" data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                <button type="button" class="btn btn-default compare" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
            </div>
        </div>

    </div>
    <br>

    <!-- Showcase -->
    <div class="row">

        <div class="col-sm-6">
            <a href=""><img src="<?php echo $AssetUrl; ?>/img/banner_sc_1.jpg" alt="Image holder" class="img-responsive showcase"></a>
        </div>

        <div class="col-sm-6">

            <div class="row">
                <div class="col-sm-12">
                    <a href=""><img src="<?php echo $AssetUrl; ?>/img/banner_sc_2.jpg" alt="Image holder" class="img-responsive showcase"></a>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <a href=""><img src="<?php echo $AssetUrl; ?>/img/banner_sc_3.jpg" alt="Image holder" class="img-responsive showcase"></a>
                </div>
            </div>

        </div>

    </div>
    <br>

    <h3>Featured</h3>
    <div class="row">

        <div class="product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-item">
                <div class="item-overlay">
                    <div class="clickable">
                        <a href="">Mochila para Laptop</a>
                    </div>
                </div>
                <div class="image">
                    <a href=""><img src="<?php echo $AssetUrl; ?>/img/product_x800_1.jpg" alt="Product 1"></a>
                </div>
                <div class="caption">
                    <div class="name">
                        <a href="">Mochila para Laptop</a>
                    </div>
                    <div class="price">
                        <span class="price-new">S./ 120.00</span>
                    </div>
                    <div class="cart">
                        <button type="button" class="btn btn-primary">Ver Detalle</button>
                    </div>
                </div>
                <button type="button" class="btn btn-default wishlist" data-toggle="tooltip" data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                <button type="button" class="btn btn-default compare" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
            </div>
        </div>

        <!-- Producto -->
        <div class="product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-item">
                <div class="item-overlay">
                    <div class="clickable">
                        <a href="">Mochila para Laptop</a>
                    </div>
                </div>
                <div class="image">
                    <a href=""><img src="<?php echo $AssetUrl; ?>/img/product_x800_1.jpg" alt="Product 1"></a>
                </div>
                <div class="caption">
                    <div class="name">
                        <a href="">Mochila para Laptop</a>
                    </div>
                    <div class="price">
                        <span class="price-new">S./ 120.00</span>
                    </div>
                    <div class="cart">
                        <button type="button" class="btn btn-primary">Ver Detalle</button>
                    </div>
                </div>
                <button type="button" class="btn btn-default wishlist" data-toggle="tooltip" data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                <button type="button" class="btn btn-default compare" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
            </div>
        </div>

        <!-- Producto -->
        <div class="product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-item">
                <div class="item-overlay">
                    <div class="clickable">
                        <a href="">Mochila para Laptop</a>
                    </div>
                </div>
                <div class="image">
                    <a href=""><img src="<?php echo $AssetUrl; ?>/img/product_x800_1.jpg" alt="Product 1"></a>
                </div>
                <div class="caption">
                    <div class="name">
                        <a href="">Mochila para Laptop</a>
                    </div>
                    <div class="price">
                        <span class="price-new">S./ 120.00</span>
                    </div>
                    <div class="cart">
                        <button type="button" class="btn btn-primary">Ver Detalle</button>
                    </div>
                </div>
                <button type="button" class="btn btn-default wishlist" data-toggle="tooltip" data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                <button type="button" class="btn btn-default compare" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
            </div>
        </div>

        <!-- Producto -->
        <div class="product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-item">
                <div class="item-overlay">
                    <div class="clickable">
                        <a href="">Mochila para Laptop</a>
                    </div>
                </div>
                <div class="image">
                    <a href=""><img src="<?php echo $AssetUrl; ?>/img/product_x800_1.jpg" alt="Product 1"></a>
                </div>
                <div class="caption">
                    <div class="name">
                        <a href="">Mochila para Laptop</a>
                    </div>
                    <div class="price">
                        <span class="price-new">S./ 120.00</span>
                    </div>
                    <div class="cart">
                        <button type="button" class="btn btn-primary">Ver Detalle</button>
                    </div>
                </div>
                <button type="button" class="btn btn-default wishlist" data-toggle="tooltip" data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                <button type="button" class="btn btn-default compare" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
            </div>
        </div>

    </div>
    <br>

    <!-- Carousel -->
    <div class="row">
        <div class="col-sm-12">
            <div class="carousel owl-carousel owl-theme">
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
                <div><img src="<?php echo $AssetUrl; ?>/img/billabong.png"></div>
            </div>
        </div>
    </div>

</div>