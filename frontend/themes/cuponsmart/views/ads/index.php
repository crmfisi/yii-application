<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use common\models\Subcategory;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AdsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cuponsmart :: Cupones destacados :: ';
$this->params['breadcrumbs'][] = $this->title;
$AssetUrl =  Yii::$app->frontend_alias->getAssetsUrl();
?>
<div class="container">

    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--    <p>
        <? /*= Html::a('Create Ads', ['create'], ['class' => 'btn btn-success']) */ ?>
    </p>-->

    <h3><?php echo isset($subcategory_id)? Subcategory::find()->where(['_id' => $subcategory_id])->one()->name : 'Cupones destacados' ; ?></h3>
    <div class="row">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index, $widget) {
                return
                    '
                        <div class="product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="product-item">
                                <div class="item-overlay">
                                    <div class="clickable">
                                        '.Html::a(Html::encode($model->title), ['view', 'id' => (string)$model->_id]).'
                                    </div>
                                </div>
                                <div class="image">
                                    '.Html::a(Html::img(Yii::$app->frontend_alias->getAssetsUrl().'/img/product_x800_1.jpg',['alt'=>'Product '.$model->_id, ]), ['view', 'id' => (string)$model->_id]).'
                                </div>
                                <div class="caption">
                                    <div class="name">
                                        <a href="">'.$model->title.'</a>
                                    </div>
                                    <div class="price">
                                        <span class="price-new">S./ '.$model->price.'</span>
                                    </div>
                                    <div class="cart">
                                        '.Html::a('<button type="button" class="btn btn-primary">Ver Detalle</button>', ['view', 'id' => (string)$model->_id]).'

                                    </div>
                                </div>
                                <button type="button" class="btn btn-default wishlist" data-toggle="tooltip" data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                                <button type="button" class="btn btn-default compare" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
                            </div>
                        </div>

                ';
                // Html::a(Html::encode($model->title), ['view', 'id' => (string)$model->_id]);
            },
        ]) ?>
    </div>
</div>
