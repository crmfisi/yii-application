<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Ads */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$AssetUrl = Yii::$app->frontend_alias->getAssetsUrl();
?>


<div class="container">
    <ul class="breadcrumb">
        <li><a href="<?php echo Url::toRoute('/'); ?>"><i class="fa fa-home"></i></a></li>
        <li><a href="#"><?php echo $model->subcategory->category->name ?></a></li>
        <li><?php echo Html::a(Html::encode($model->subcategory->name),
                [
                    'filter_subcategory',
                    'subcategory' => (string)$model->subcategory->_id
                ]) ?></li>
        <li><a href="#"><?php echo $model->title ?></a></li>
    </ul>

    <div class="row">
        <div class="sidebar col-sm-3 hidden-xs">

            <div class="list-group">
                <a href="grid.html" class="list-group-item">Lorem ipsum dolor</a>
                <a href="grid.html" class="list-group-item">- Lorem ipsum dolor sit amet</a>
                <a href="grid.html" class="list-group-item">- Lorem ipsum dolor</a>
                <a href="grid.html" class="list-group-item">Lorem ipsum dolor</a>
                <a href="grid.html" class="list-group-item">Lorem ipsum dolor sit amet</a>
                <a href="grid.html" class="list-group-item">Lorem ipsum dolor sit amet</a>
                <a href="grid.html" class="list-group-item">Lorem ipsum dolor sit</a>
                <a href="grid.html" class="list-group-item">- Lorem ipsum dolor sit amet</a>
                <a href="grid.html" class="list-group-item">- Lorem ipsum dolor sit amet</a>
                <a href="grid.html" class="list-group-item">Lorem ipsum dolor sit</a>
            </div>

            <div class="product-sidebar">
                <!-- Featured Product Item -->
                <div class="product-item">
                    <div class="image">
                        <a href="#"><img src="<?php echo $AssetUrl ?>/img/product_x800_3.jpg" alt="Product 4"></a>
                    </div>
                    <div class="name">
                        <a href="#">Leather laptop bag</a>
                    </div>
                    <div class="price">
                        <span>$722.00</span>
                    </div>
                </div>
                <!-- Featured Product Item -->
                <div class="product-item">
                    <div class="image">
                        <a href="#"><img src="<?php echo $AssetUrl ?>/img/product_x800_3.jpg" alt="Product 4"></a>
                    </div>
                    <div class="name">
                        <a href="#">Leather laptop bag</a>
                    </div>
                    <div class="price">
                        <span>$722.00</span>
                    </div>
                </div>

            </div>

        </div>

        <div class="content col-sm-9">
            <div class="row">
                <!-- DETAIL SIDEBAR -->
                <div class="col-sm-6">
                    <ul class="thumbnails">
                        <li><a class="thumbnail"
                               href="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"><img
                                    src="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"
                                    alt="Product Image"
                                    class="img-rounded"></a></li>
                        <li class="image-additional"><a class="thumbnail"
                                                        href="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"><img
                                    src="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"
                                    alt="Additional Image 1"></a></li>
                        <li class="image-additional"><a class="thumbnail"
                                                        href="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"><img
                                    src="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"
                                    alt="Additional Image 2"></a></li>
                        <li class="image-additional"><a class="thumbnail"
                                                        href="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"><img
                                    src="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"
                                    alt="Additional Image 3"></a></li>
                        <li class="image-additional"><a class="thumbnail"
                                                        href="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"><img
                                    src="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"
                                    alt="Additional Image 4"></a></li>
                    </ul>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-description" data-toggle="tab">Descripción</a></li>
                        <li><a href="#tab-specification" data-toggle="tab">Especificación</a></li>
                        <li><a href="#tab-review" data-toggle="tab">Comentarios</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-description">
                            <p><?php echo $model->description; ?></p>
                        </div>
                        <!--<div class="tab-pane" id="tab-specification">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th colspan="2">Memory</th>
                                </tr>
                                <tr>
                                    <td>Test 1</td>
                                    <td>16Gb</td>
                                </tr>
                                <tr>
                                    <th colspan="2">Processor</th>
                                </tr>
                                <tr>
                                    <td>No. of Cores</td>
                                    <td>4</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab-review">
                            <form class="form-horizontal" id="form-review">
                                <div id="review">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td style="width: 50%;"><strong>Arrow</strong></td>
                                            <td class="text-right">08/05/2015</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p>Texto de prueba</p>
                                                <span class="fa fa-stack"><i class="fa fa-star"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o"></i></span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h2>Comentarios</h2>

                                <div class="form-group required">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="input-name">Tu Nombre</label>
                                        <input type="text" name="name" value="" id="input-name"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="input-review">Escribe tu
                                            comentario</label>
                                        <textarea name="text" rows="5" id="input-review"
                                                  class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <div class="col-sm-12">
                                        <label class="control-label">Rating</label>
                                        &nbsp;&nbsp;&nbsp; Malo &nbsp;
                                        <input type="radio" name="rating" value="1">
                                        &nbsp;
                                        <input type="radio" name="rating" value="2">
                                        &nbsp;
                                        <input type="radio" name="rating" value="3">
                                        &nbsp;
                                        <input type="radio" name="rating" value="4">
                                        &nbsp;
                                        <input type="radio" name="rating" value="5">
                                        &nbsp; Bueno
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="g-recaptcha" data-sitekey=""></div>
                                    </div>
                                </div>
                                <div class="buttons clearfix">
                                    <div class="pull-right">
                                        <button type="button" id="button-review" data-loading-text="Cargando..."
                                                class="btn btn-primary">Continuar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>-->
                    </div>
                </div>
                <!-- END DETAIL SIDEBAR -->
                <div class="col-sm-6">
                    <div class="btn-group">
                        <button type="button" data-toggle="tooltip" class="btn btn-default" title="Wishlist"><i
                                class="fa fa-heart"></i></button>
                        <button type="button" data-toggle="tooltip" class="btn btn-default" title="Compare"><i
                                class="fa fa-circle-o"></i></button>
                    </div>
                    <h1><?php echo $model->title; ?></h1>
                    <ul class="list-unstyled">
                        <li>Marca: <a href="#<?php ?>"><?php ?></a></li>
                        <li>Codigo de producto: <?php ?></li>
                        <li>Puntos cuponsmart: <?php ?></li>
                        <li>Disponibilidad: <?php ?></li>
                    </ul>
                    <ul class="list-unstyled">
                        <li><h2><?php echo $model->currency->symbol . ' ' . $model->price ?></h2></li>
                        <li>Total con
                            impuestos: <?php echo $model->currency->symbol . ' ' . ($model->price * 1.18) ?></li>
                        <li>Precio con puntos cuponsmart: <?php echo round($model->price * 12.5, 0) ?></li>
                    </ul>
                    <div id="product">
                        <hr>
                        <h3>Opciones Disponibles</h3>

                        <div class="form-group">
                            <label class="control-label" for="input-option-date">Fecha Delivery</label>

                            <div class="input-group date">
                                <input type="text" name="date" value="" data-date-format="YYYY-MM-DD"
                                       id="input-option-date" class="form-control">
                                    <span class="input-group-btn">
                                      <button class="btn btn-default" type="button">
                                          <i class="fa fa-calendar"></i>
                                      </button>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="input-quantity">Cantidad</label>
                            <input type="text" name="quantity" value="1" size="2" id="input-quantity"
                                   class="form-control">
                            <br>
                            <button type="button" id="button-cart" data-loading-text="Loading..."
                                    class="btn btn-primary btn-block">Comprar
                            </button>
                        </div>
                        <!--<div class="alert alert-info"><i class="fa fa-info-circle"></i> Mensaje de Alerta</div>-->
                    </div>

                    <!-- Tags -->
                    <p class="tags">Tags:
                        <a href="#"><?php echo $model->subcategory->name ?></a>,
                        <!--<a href="#">Shoes</a>,
                        <a href="#">Shoes</a>,
                        <a href="#">Shoes</a>,-->
                    </p>

                </div>
            </div>
            <h3>Productos Relacionados</h3>

            <div class="row">

                <!-- Product Item -->
                <div class="product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="product-item">
                        <div class="item-overlay">
                            <div class="clickable">
                                <a href="product.html">Leather laptop bag navy</a>
                            </div>
                        </div>
                        <div class="image">
                            <a href="#"><img
                                    src="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"
                                    alt="Product 1"></a>
                        </div>
                        <div class="caption">
                            <div class="name">
                                <a href="#">Leather laptop bag navy</a>
                            </div>
                            <div class="price">
                                <span>$866.00</span>
                            </div>
                            <div class="cart">
                                <button type="button" class="btn btn-primary">Add to Cart</button>
                            </div>
                        </div>
                        <button type="button" class="btn btn-default wishlist" data-toggle="tooltip"
                                data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                        <button type="button" class="btn btn-default compare" data-toggle="tooltip"
                                data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
                    </div>
                </div>
                <!-- Product Item -->
                <div class="product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="product-item">
                        <div class="item-overlay">
                            <div class="clickable">
                                <a href="product.html">Leather laptop bag navy</a>
                            </div>
                        </div>
                        <div class="image">
                            <a href="#"><img
                                    src="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"
                                    alt="Product 1"></a>
                        </div>
                        <div class="caption">
                            <div class="name">
                                <a href="#">Leather laptop bag navy</a>
                            </div>
                            <div class="price">
                                <span>$866.00</span>
                            </div>
                            <div class="cart">
                                <button type="button" class="btn btn-primary">Add to Cart</button>
                            </div>
                        </div>
                        <button type="button" class="btn btn-default wishlist" data-toggle="tooltip"
                                data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                        <button type="button" class="btn btn-default compare" data-toggle="tooltip"
                                data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
                    </div>
                </div>
                <!-- Product Item -->
                <div class="product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="product-item">
                        <div class="item-overlay">
                            <div class="clickable">
                                <a href="product.html">Leather laptop bag navy</a>
                            </div>
                        </div>
                        <div class="image">
                            <a href="#"><img
                                    src="<?php echo $AssetUrl ?>/img/product_x800_3.jpg"
                                    alt="Product 1"></a>
                        </div>
                        <div class="caption">
                            <div class="name">
                                <a href="#">Leather laptop bag navy</a>
                            </div>
                            <div class="price">
                                <span>$866.00</span>
                            </div>
                            <div class="cart">
                                <button type="button" class="btn btn-primary">Add to Cart</button>
                            </div>
                        </div>
                        <button type="button" class="btn btn-default wishlist" data-toggle="tooltip"
                                data-placement="right" title="Wishlist"><i class="fa fa-heart"></i></button>
                        <button type="button" class="btn btn-default compare" data-toggle="tooltip"
                                data-placement="right" title="Compare"><i class="fa fa-circle-o"></i></button>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>


