<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class AppAsset extends AssetBundle
{
//    public $basePath = '@webroot';
//    public $baseUrl = '@web';
    public $sourcePath = '@frontend/themes/cuponsmart/assets';
    public $css = [
        // 'css/site.css',
        'css/bootstrap.min.css',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
        '/fonts.googleapis.com/css?family=Roboto:400,300,500,700',
        'js/owl-carousel/owl.carousel.css',
        'css/main.css',
        'css/cuponsmart.css'
    ];
    public $js = [
        'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js',
        'js/bootstrap.min.js',
        'js/owl-carousel/owl.carousel.min.js'
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
