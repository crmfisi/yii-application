<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@backend/themes/urban/assets';
    public $css = [
        // 'css/site.css',
        'vendor/bootstrap/dist/css/bootstrap.css',
        'vendor/perfect-scrollbar/css/perfect-scrollbar.css',
        'styles/roboto.css',
        'styles/font-awesome.css',
        'styles/panel.css',
        'styles/feather.css',
        'styles/animate.css',
        'styles/urban.css',
        'styles/cuponsmart.css',
        'styles/urban.skins.css'
    ];
    public $js = [
        'scripts/extentions/modernizr.js',
        'vendor/jquery/dist/jquery.js',
        'vendor/bootstrap/dist/js/bootstrap.js',
        'vendor/jquery.easing/jquery.easing.js',
        'vendor/fastclick/lib/fastclick.js',
        'vendor/onScreen/jquery.onscreen.js',
        'vendor/jquery-countTo/jquery.countTo.js',
        'vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
        'scripts/ui/accordion.js',
        'scripts/ui/animate.js',
        'scripts/ui/link-transition.js',
        'scripts/ui/panel-controls.js',
        'scripts/ui/preloader.js',
        'scripts/ui/toggle.js',
        'scripts/urban-constants.js',
        'scripts/extentions/lib.js',
        'vendor/datatables/media/js/jquery.dataTables.js',
        'scripts/extentions/bootstrap-datatables.js',
        'scripts/pages/table-edit.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        /*'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',*/
    ];
}
