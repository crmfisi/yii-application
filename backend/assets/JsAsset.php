<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class JsAsset extends AssetBundle
{
    public $sourcePath = '@backend/themes/urban/assets';


    public $js = [
        'http://code.jquery.com/jquery-1.10.0.min.js',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
    public $depends = [
        // SE ELIMINA EL CONTENIDO QUE ESTA EN ESTE ARRAY PARA MEJOR MANEJO DEL TEMPLATE
    ];
    public $jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];

}
