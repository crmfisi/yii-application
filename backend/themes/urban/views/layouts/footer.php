<?php
/**
 * User created: Cesar Mamani Dominguez
 * Date: 25/01/2016
 * Time: 02:40 PM
 */

?>
    </div>
</div>

<!-- bottom footer -->
<footer class="content-footer">

    <nav class="footer-right">
        <ul class="nav">
            <li>
                <a href="javascript:;">Feedback</a>
            </li>
            <li>
                <a href="javascript:;" class="scroll-up">
                    <i class="fa fa-angle-up"></i>
                </a>
            </li>
        </ul>
    </nav>

    <nav class="footer-left">
        <ul class="nav">
            <li>
                <a href="javascript:;">Copyright <i class="fa fa-copyright"></i> <span>Urban</span>
                    2015. All rights reserved</a>
            </li>
            <li>
                <a href="javascript:;">Careers</a>
            </li>
            <li>
                <a href="javascript:;">
                    Privacy Policy
                </a>
            </li>
        </ul>
    </nav>

</footer>