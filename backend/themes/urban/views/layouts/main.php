<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use backend\assets\JsAsset;

// @var $this \yii\web\View
/* @var $content string */
AppAsset::register($this);
JsAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!doctype html>
<html class="no-js" lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <meta name="description" content="">
        <link rel="shortcut icon" href="/favicon.ico">
        <?php $this->head() ?>
    </head>

    <style>
        .brand .title-admin {
            font-size: 1.1814em;
            color: #E0E8F2;
        }
    </style>
<body>

<?php $this->beginBody() ?>
<?php echo $this->renderFile('@backend/themes/urban/views/layouts/header.php'); ?>
<?php echo $content; ?>
<?php echo $this->renderFile('@backend/themes/urban/views/layouts/footer.php'); ?>


<?php $this->endBody() ?>

<?php $this->endPage() ?>

</body>

</html>
