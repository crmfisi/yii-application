<?php
/**
 * User created: Cesar Mamani Dominguez
 * Date: 25/01/2016
 * Time: 02:46 PM
 */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

$current_route = Yii::$app->request->resolve()[0];
// var_dump($current_route); die;
?>
<div class="quick-launch-panel">
    <div class="container">
        <div class="quick-launcher-inner">
            <a href="javascript:;" class="close" data-toggle="quick-launch-panel">�</a>

            <div class="css-table-xs">
                <div class="col">
                    <a href="app-calendar.html">
                        <i class="icon-marquee"></i>
                        <span>Calendar</span>
                    </a>
                </div>
                <div class="col">
                    <a href="app-gallery.html">
                        <i class="icon-drop"></i>
                        <span>Gallery</span>
                    </a>
                </div>
                <div class="col">
                    <a href="app-messages.html">
                        <i class="icon-mail"></i>
                        <span>Messages</span>
                    </a>
                </div>
                <div class="col">
                    <a href="app-social.html">
                        <i class="icon-speech-bubble"></i>
                        <span>Social</span>
                    </a>
                </div>
                <div class="col">
                    <a href="charts-flot.html">
                        <i class="icon-pie-graph"></i>
                        <span>Analytics</span>
                    </a>
                </div>
                <div class="col">
                    <a href="javascript:;">
                        <i class="icon-esc"></i>
                        <span>Documentation</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /quick launch panel -->

<div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

        <div class="brand">

            <!-- logo -->
            <div class="brand-logo title-admin">CRM FISI
                <!--<img src="images/logo.png" height="15" alt="">-->
            </div>
            <!-- /logo -->

            <!-- toggle small sidebar menu -->
            <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </a>
            <!-- /toggle small sidebar menu -->

        </div>

        <!-- main navigation -->
        <nav role="navigation">

            <ul class="nav">

                <!-- dashboard -->
                <li class=" <?php echo ($current_route == '') ? 'open' : '' ?>">
                    <a href="<?php echo Url::toRoute('/'); ?>">
                        <i class="fa fa-flask"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <!-- /dashboard -->

                <li class="menu-accordion">
                    <a href="javascript:;">
                        <i class="fa fa-toggle-on"></i>
                        <span>Menu de operacion</span>
                    </a>
                    <ul class="sub-menu">
                        <li class="">
                            <a href="<?php echo Url::toRoute('/operacion'); ?>">
                                <span class="title">Cambio</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo Url::toRoute('/operacion'); ?>">
                                <span class="title">Canje</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo Url::toRoute('/operacion'); ?>">
                                <span class="title">Devolucion</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo Url::toRoute('/operacion'); ?>">
                                <span class="title">Anulacion</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo Url::toRoute('/operacion'); ?>">
                                <span class="title">Quejas</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo Url::toRoute('/operacion'); ?>">
                                <span class="title">Sujerencias</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo Url::toRoute('/operacion'); ?>">
                                <span class="title">Reclamos</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- ui -->
                <li class="menu-accordion <?php echo (
                    $current_route == 'producto' || $current_route == 'producto/create' || $current_route == 'producto/edit' || $current_route == 'producto/view' || $current_route == 'producto/delete' ||
                    $current_route == 'catalogo_producto' || $current_route == 'catalogo_producto/create' || $current_route == 'catalogo_producto/edit' || $current_route == 'catalogo_producto/view' || $current_route == 'catalogo_producto/delete' ||
                    $current_route == 'catalogo' || $current_route == 'catalogo/create' || $current_route == 'catalogo/edit' || $current_route == 'catalogo/view' || $current_route == 'catalogo/delete' ||
                    $current_route == 'familia' || $current_route == 'familia/create' || $current_route == 'familia/edit' || $current_route == 'familia/view' || $current_route == 'familia/delete' ||
                    $current_route == 'campana' || $current_route == 'campana/create' || $current_route == 'campana/edit' || $current_route == 'campana/view' || $current_route == 'campana/delete' ||
                    $current_route == 'incentivo' || $current_route == 'incentivo/create' || $current_route == 'incentivo/edit' || $current_route == 'productcatalog/location/view' || $current_route == 'incentivo/delete' ||
                    $current_route == 'tipo_incentivo' || $current_route == 'tipo_incentivo/create' || $current_route == 'tipo_incentivo/edit' || $current_route == 'tipo_incentivo/view' || $current_route == 'tipo_incentivo/delete' ||
                    $current_route == 'puntaje' || $current_route == 'puntaje/create' || $current_route == 'puntaje/edit' || $current_route == 'productcatalog/department/view' || $current_route == 'puntaje/delete' ||
                    $current_route == 'interlocutor_comercial' || $current_route == 'interlocutor_comercial/create' || $current_route == 'interlocutor_comercial/edit' || $current_route == 'productcatalog/province/view' || $current_route == 'interlocutor_comercial/delete' ||
                    $current_route == 'zona' || $current_route == 'zona/create' || $current_route == 'productcatalogzona/edit' || $current_route == 'zona/view' || $current_route == 'zona/delete' ||
                    $current_route == 'distrito' || $current_route == 'distrito/create' || $current_route == 'distrito/edit' || $current_route == 'distrito/view' || $current_route == 'distrito/delete' ||
                    $current_route == 'roles' || $current_route == 'roles/create' || $current_route == 'roles/edit' || $current_route == 'roles/view' || $current_route == 'roles/delete' ||
                    $current_route == 'operacion' || $current_route == 'operacion/create' || $current_route == 'operacion/edit' || $current_route == 'operacion/view' || $current_route == 'operacion/delete' ||
                    $current_route == 'operacion_detalle' || $current_route == 'operacion_detalle/create' || $current_route == 'operacion_detalle/edit' || $current_route == 'operacion_detalle/view' || $current_route == 'operacion_detalle/delete' ||
                    $current_route == 'pedido' || $current_route == 'pedido/create' || $current_route == 'pedido/edit' || $current_route == 'pedido/view' || $current_route == 'pedido/delete'
                ) ? 'open' : '' ?>">
                    <a href="javascript:;">
                        <i class="fa fa-toggle-on"></i>
                        <span>Menu de configuración</span>
                    </a>
                    <ul class="sub-menu">
                        <li class="<?php echo ($current_route == 'producto' || $current_route == 'producto/create' || $current_route == 'producto/edit' || $current_route == 'producto/view' || $current_route == 'producto/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/producto'); ?>">
                                <span class="title">Productos</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'catalogo_producto' || $current_route == 'catalogo_producto/create' || $current_route == 'catalogo_producto/edit' || $current_route == 'catalogo_producto/view' || $current_route == 'catalogo_producto/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/catalogo_producto'); ?>">
                                <span>Catalogo producto</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'catalogo' || $current_route == 'catalogo/create' || $current_route == 'catalogo/edit' || $current_route == 'catalogo/view' || $current_route == 'catalogo/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/catalogo'); ?>">
                                <span>Catalogo</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'familia' || $current_route == 'familia/create' || $current_route == 'familia/edit' || $current_route == 'familia/view' || $current_route == 'familia/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/familia'); ?>">
                                <span>Familia</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'campana' || $current_route == 'campana/create' || $current_route == 'campana/edit' || $current_route == 'campana/view' || $current_route == 'campana/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/campana'); ?>">
                                <span>Campaña</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'incentivo' || $current_route == 'incentivo/create' || $current_route == 'incentivo/edit' || $current_route == 'productcatalog/location/view' || $current_route == 'incentivo/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/incentivo'); ?>">
                                <span>Incentivo</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'tipo_incentivo' || $current_route == 'tipo_incentivo/create' || $current_route == 'tipo_incentivo/edit' || $current_route == 'tipo_incentivo/view' || $current_route == 'tipo_incentivo/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/tipo_incentivo'); ?>">
                                <span>Tipo incentivo</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'puntaje' || $current_route == 'puntaje/create' || $current_route == 'puntaje/edit' || $current_route == 'productcatalog/department/view' || $current_route == 'puntaje/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/puntaje'); ?>">
                                <span>Puntaje</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'interlocutor_comercial' || $current_route == 'interlocutor_comercial/create' || $current_route == 'interlocutor_comercial/edit' || $current_route == 'productcatalog/province/view' || $current_route == 'interlocutor_comercial/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/interlocutor_comercial'); ?>">
                                <span>Interlocutor comercial</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'zona' || $current_route == 'zona/create' || $current_route == 'productcatalogzona/edit' || $current_route == 'zona/view' || $current_route == 'zona/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/zona'); ?>">
                                <span>Zona</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'distrito' || $current_route == 'distrito/create' || $current_route == 'distrito/edit' || $current_route == 'distrito/view' || $current_route == 'distrito/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/distrito'); ?>">
                                <span>Distrito</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'roles' || $current_route == 'roles/create' || $current_route == 'roles/edit' || $current_route == 'roles/view' || $current_route == 'roles/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/roles'); ?>">
                                <span>Roles</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'operacion' || $current_route == 'operacion/create' || $current_route == 'operacion/edit' || $current_route == 'operacion/view' || $current_route == 'operacion/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/operacion'); ?>">
                                <span>Operacion</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'operacion_detalle' || $current_route == 'operacion_detalle/create' || $current_route == 'operacion_detalle/edit' || $current_route == 'operacion_detalle/view' || $current_route == 'operacion/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/operacion_detalle'); ?>">
                                <span>Operacion detalle</span>
                            </a>
                        </li>
                        <li class="<?php echo ($current_route == 'pedido' || $current_route == 'pedido/create' || $current_route == 'pedido/edit' || $current_route == 'pedido/view' || $current_route == 'pedido/delete') ? 'active' : '' ?>">
                            <a href="<?php echo Url::toRoute('/pedido'); ?>">
                                <span>Pedido</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- /ui -->


            </ul>
        </nav>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

        <!-- top header -->
        <header class="header navbar">

            <div class="brand visible-xs">
                <!-- toggle offscreen menu -->
                <div class="toggle-offscreen">
                    <a href="#" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <!-- /toggle offscreen menu -->

                <!-- logo -->
                <div class="brand-logo">
                    <img src="images/logo-dark.png" height="15" alt="">
                </div>
                <!-- /logo -->

            </div>

            <ul class="nav navbar-nav hidden-xs">
                <li>
                    <p class="navbar-text"><?= Html::encode($this->title) ?></p>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right hidden-xs">
                <li>
                    <a href="javascript:;" data-toggle="quick-launch-panel">
                        <i class="fa fa-circle-thin"></i>
                    </a>
                </li>

                <li>
                    <a href="javascript:;" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>

                        <div class="status bg-danger border-danger animated bounce"></div>
                    </a>
                    <ul class="dropdown-menu notifications">
                        <li class="notifications-header">
                            <p class="text-muted small">You have 3 new messages</p>
                        </li>
                        <li>
                            <ul class="notifications-list">
                                <li>
                                    <a href="javascript:;">
                      <span class="pull-left mt2 mr15">
                                                <img src="images/avatar.jpg" class="avatar avatar-xs img-circle" alt="">
                                            </span>

                                        <div class="overflow-hidden">
                                            <span>Sean launched a new application</span>
                                            <span class="time">2 seconds ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <div class="pull-left mt2 mr15">
                                            <div class="circle-icon bg-danger">
                                                <i class="fa fa-chain-broken"></i>
                                            </div>
                                        </div>
                                        <div class="overflow-hidden">
                                            <span>Removed chrome from app list</span>
                                            <span class="time">4 Hours ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                      <span class="pull-left mt2 mr15">
                                                <img src="images/face3.jpg" class="avatar avatar-xs img-circle" alt="">
                                            </span>

                                        <div class="overflow-hidden">
                                            <span class="text-muted">Jack Hunt has registered</span>
                                            <span class="time">9 hours ago</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="notifications-footer">
                            <a href="javascript:;">See all messages</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:;" data-toggle="dropdown">
                        <img src="images/avatar.jpg" class="header-avatar img-circle ml10" alt="user" title="user">
                        <span class="pull-left">admin@cmr.com</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="javascript:;">Settings</a>
                        </li>
                        <li>
                            <a href="javascript:;">Upgrade</a>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <span class="label bg-danger pull-right">34</span>
                                <span>Notifications</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;">Help</a>
                        </li>
                        <li>
                            <a href="signin.html">Logout</a>
                        </li>
                    </ul>

                </li>

                <!--<li>
                  <a href="javascript:;" class="hamburger-icon v2" data-toggle="layout-chat-open">
                    <span></span>
                    <span></span>
                    <span></span>
                  </a>
                </li>-->
            </ul>
        </header>
        <!-- /top header -->

        <div class="main-content">
