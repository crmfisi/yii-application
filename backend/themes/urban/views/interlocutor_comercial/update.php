<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Interlocutor_comercial */

$this->title = Yii::t('interlocutor_comercial', 'Update {modelClass}: ', [
    'modelClass' => 'Interlocutor Comercial',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('interlocutor_comercial', 'Interlocutor Comercials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('interlocutor_comercial', 'Update');
?>
<div class="interlocutor-comercial-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
