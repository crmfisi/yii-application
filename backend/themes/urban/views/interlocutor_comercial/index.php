<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\Interlocutor_comercialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('interlocutor_comercial', 'Interlocutor Comercials');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="interlocutor-comercial-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('interlocutor_comercial', 'Create Interlocutor Comercial'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'zona_id',
            'roles_id',
            'codigo',
            'nombre',
            // 'apellido',
            // 'email:email',
            // 'telefono',
            // 'estado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
