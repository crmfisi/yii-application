<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Interlocutor_comercial */

$this->title = Yii::t('interlocutor_comercial', 'Create Interlocutor Comercial');
$this->params['breadcrumbs'][] = ['label' => Yii::t('interlocutor_comercial', 'Interlocutor Comercials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="interlocutor-comercial-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
