<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Interlocutor_comercial */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('interlocutor_comercial', 'Interlocutor Comercials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="interlocutor-comercial-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('interlocutor_comercial', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('interlocutor_comercial', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('interlocutor_comercial', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'zona_id',
            'roles_id',
            'codigo',
            'nombre',
            'apellido',
            'email:email',
            'telefono',
            'estado',
        ],
    ]) ?>

</div>
