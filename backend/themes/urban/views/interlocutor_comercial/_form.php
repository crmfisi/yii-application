<?php

use yii\helpers\Html; // CABECERA PARA USO DE LA FUNCION HTML:ENCODE
use yii\bootstrap\ActiveForm; // CABECERA PARA USO DE FORMULARIOS EN LA PAGINA
use yii\helpers\ArrayHelper; // CABECERA PARA USO DE ARRAYS A MUCHOS TIPOS DE ESCALA
use backend\models\Zona;
use backend\models\Roles;

/* @var $this yii\web\View */
/* @var $model backend\models\Interlocutor_comercial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-body ">
    <div class="row no-margin">
        <div class="col-lg-12">

            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal bordered-group'],
                'fieldConfig' => [
                    'template' => '{label}<div class="col-sm-10 col-lg-8">{input}{hint}{error}</div>',
                    'labelOptions' => [
                        'class' => 'col-sm-2 control-label'
                    ]
                ]
            ]); ?>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Zona</label>
                <div class="col-sm-10 col-lg-8">
                    <?php $field = $form->field($model,
                        'zona_id')->dropDownList(ArrayHelper::map(Zona::find()->orderBy('nombre')->all(),
                        'id', 'nombre'), [
                        'class' => 'form-control',
                        'prompt' => Yii::t('app', 'Seleccionar')])->label(Yii::t('app',
                        ''));
                    echo $field;
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Roles</label>
                <div class="col-sm-10 col-lg-8">
                    <?php $field = $form->field($model,
                        'roles_id')->dropDownList(ArrayHelper::map(Roles::find()->orderBy('nombre')->all(),
                        'id', 'nombre'), [
                        'class' => 'form-control',
                        'prompt' => Yii::t('app', 'Seleccionar')])->label(Yii::t('app',
                        ''));
                    echo $field;
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>
            </div>
            <!--            < ?= $form->field($model, 'estado')->textInput() ?>-->

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('interlocutor_comercial', 'Create') : Yii::t('interlocutor_comercial', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
