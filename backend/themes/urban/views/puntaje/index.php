<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PuntajeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('puntaje', 'Puntajes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puntaje-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('puntaje', 'Create Puntaje'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'interlocutor_comercial_id',
            'puntaje',
            'estado',
            'fecha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
