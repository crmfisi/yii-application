<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Puntaje */

$this->title = Yii::t('puntaje', 'Update {modelClass}: ', [
    'modelClass' => 'Puntaje',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('puntaje', 'Puntajes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('puntaje', 'Update');
?>
<div class="puntaje-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
