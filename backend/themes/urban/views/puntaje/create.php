<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Puntaje */

$this->title = Yii::t('puntaje', 'Create Puntaje');
$this->params['breadcrumbs'][] = ['label' => Yii::t('puntaje', 'Puntajes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puntaje-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
