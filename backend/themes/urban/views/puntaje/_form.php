<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Puntaje */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="puntaje-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'interlocutor_comercial_id')->textInput() ?>

    <?= $form->field($model, 'puntaje')->textInput() ?>

    <?= $form->field($model, 'estado')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('puntaje', 'Create') : Yii::t('puntaje', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
