<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Tipo_operacion */

$this->title = Yii::t('tipo_operacion', 'Create Tipo Operacion');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tipo_operacion', 'Tipo Operacions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-operacion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
