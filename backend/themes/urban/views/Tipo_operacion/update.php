<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Tipo_operacion */

$this->title = Yii::t('tipo_operacion', 'Update {modelClass}: ', [
    'modelClass' => 'Tipo Operacion',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('tipo_operacion', 'Tipo Operacions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('tipo_operacion', 'Update');
?>
<div class="tipo-operacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
