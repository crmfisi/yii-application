<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Catalogo_producto */

$this->title = Yii::t('catalogo_producto', 'Update {modelClass}: ', [
    'modelClass' => 'Catalogo Producto',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalogo_producto', 'Catalogo Productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('catalogo_producto', 'Update');
?>
<div class="catalogo-producto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
