<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Catalogo_producto */

$this->title = Yii::t('catalogo_producto', 'Create Catalogo Producto');
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalogo_producto', 'Catalogo Productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogo-producto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
