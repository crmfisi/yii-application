<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\Catalogo_productoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('catalogo_producto', 'Catalogo Productos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogo-producto-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('catalogo_producto', 'Create Catalogo Producto'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'catalogo_id',
            'campana_id',
            'producto_id',
            'incentivo_id',
            // 'descripcion',
            // 'estado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
