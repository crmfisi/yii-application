<?php

use yii\helpers\Html; // CABECERA PARA USO DE LA FUNCION HTML:ENCODE
use yii\bootstrap\ActiveForm; // CABECERA PARA USO DE FORMULARIOS EN LA PAGINA
use yii\helpers\ArrayHelper; // CABECERA PARA USO DE ARRAYS A MUCHOS TIPOS DE ESCALA
use backend\models\Familia;

/* @var $this yii\web\View */
/* @var $model backend\models\Producto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-body ">

    <div class="row no-margin">
        <div class="col-lg-12">

            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal bordered-group'],
                'fieldConfig' => [
                    'template' => '{label}<div class="col-sm-10 col-lg-8">{input}{hint}{error}</div>',
                    'labelOptions' => [
                        'class' => 'col-sm-2 control-label'
                    ]
                ]
            ]); ?>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Familia</label>
                <div class="col-sm-10 col-lg-8">
                    <?php $field = $form->field($model,
                        'familia_id')->dropDownList(ArrayHelper::map(Familia::find()->orderBy('nombre')->all(),
                        'id', 'nombre'), [
                        'class' => 'form-control',
                        'prompt' => Yii::t('app', 'Seleccionar')])->label(Yii::t('app',
                        ''));
                    echo $field;
                    ?>
                </div>
            </div>

            <div class="form-group">
                <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'unidad')->textInput() ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'precio')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'precio_vta')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'descuento')->textInput() ?>
            </div>
<!--            < ?= $form->field($model, 'estado')->textInput() ?>-->

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('familia', 'Create') : Yii::t('familia', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
