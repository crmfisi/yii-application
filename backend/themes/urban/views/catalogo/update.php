<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Catalogo */

$this->title = Yii::t('catalogo', 'Update {modelClass}: ', [
    'modelClass' => 'Catalogo',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalogo', 'Catalogos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('catalogo', 'Update');
?>
<div class="catalogo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
