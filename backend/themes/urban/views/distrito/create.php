<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Distrito */

$this->title = Yii::t('distrito', 'Create Distrito');
$this->params['breadcrumbs'][] = ['label' => Yii::t('distrito', 'Distritos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distrito-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
