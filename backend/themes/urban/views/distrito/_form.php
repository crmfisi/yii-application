<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Distrito */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-body ">
    <div class="row no-margin">
        <div class="col-lg-12">

            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal bordered-group'],
                'fieldConfig' => [
                    'template' => '{label}<div class="col-sm-10 col-lg-8">{input}{hint}{error}</div>',
                    'labelOptions' => [
                        'class' => 'col-sm-2 control-label'
                    ]
                ]
            ]); ?>

            <div class="form-group">
                <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'latitud')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'longitud')->textInput(['maxlength' => true]) ?>
            </div>

            <!--    < ?= $form->field($model, 'estado')->textInput() ?>-->

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('distrito', 'Create') : Yii::t('distrito', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
