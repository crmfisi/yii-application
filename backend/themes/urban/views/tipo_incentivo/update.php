<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Tipo_incentivo */

$this->title = Yii::t('tipo_incentivo', 'Update {modelClass}: ', [
    'modelClass' => 'Tipo Incentivo',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('tipo_incentivo', 'Tipo Incentivos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('tipo_incentivo', 'Update');
?>
<div class="tipo-incentivo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
