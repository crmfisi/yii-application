<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Tipo_incentivo */

$this->title = Yii::t('tipo_incentivo', 'Create Tipo Incentivo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tipo_incentivo', 'Tipo Incentivos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-incentivo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
