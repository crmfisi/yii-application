<?php

use yii\helpers\Html; // CABECERA PARA USO DE LA FUNCION HTML:ENCODE
use yii\bootstrap\ActiveForm; // CABECERA PARA USO DE FORMULARIOS EN LA PAGINA
use yii\helpers\ArrayHelper; // CABECERA PARA USO DE ARRAYS A MUCHOS TIPOS DE ESCALA
use backend\models\Tipo_operacion;
use backend\models\Pedido;

/* @var $this yii\web\View */
/* @var $model backend\models\Operacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-body ">
    <div class="row no-margin">
        <div class="col-lg-12">

            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal bordered-group'],
                'fieldConfig' => [
                    'template' => '{label}<div class="col-sm-10 col-lg-8">{input}{hint}{error}</div>',
                    'labelOptions' => [
                        'class' => 'col-sm-2 control-label'
                    ]
                ]
            ]); ?>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Tipo_operacion</label>
                <div class="col-sm-10 col-lg-8">
                    <?php $field = $form->field($model,
                        'tipo_operacion_id')->dropDownList(ArrayHelper::map(Tipo_operacion::find()->orderBy('detalle')->all(),
                        'id', 'detalle'), [
                        'class' => 'form-control',
                        'prompt' => Yii::t('app', 'Seleccionar')])->label(Yii::t('app',
                        ''));
                    echo $field;
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Pedido</label>
                <div class="col-sm-10 col-lg-8">
                    <?php $field = $form->field($model,
                        'pedido_id')->dropDownList(ArrayHelper::map(Pedido::find()->orderBy('detalle')->all(),
                        'id', 'detalle'), [
                        'class' => 'form-control',
                        'prompt' => Yii::t('app', 'Seleccionar')])->label(Yii::t('app',
                        ''));
                    echo $field;
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'codigo_operacion')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'monto_operacion')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'estado')->textInput() ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'observacion')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'fecha_operacion')->textInput() ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('operacion', 'Create') : Yii::t('operacion', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
