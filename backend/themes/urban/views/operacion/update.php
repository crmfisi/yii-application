<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Operacion */

$this->title = Yii::t('operacion', 'Update {modelClass}: ', [
    'modelClass' => 'Operacion',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('operacion', 'Operacions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('operacion', 'Update');
?>
<div class="operacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
