<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OperacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('operacion', 'Operaciones');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacion-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<p>
        < ?= Html::a(Yii::t('operacion', 'Create Operacion'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class'           => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    return ['value' => $model->id];
                }
            ],

            // PEDIDO
            'id',
            'tipo_pedido_id',
            'interlocutor_comercial_id',
            'cantidad_producto',
            'descripcion__producto',
            // 'precio_unitario',
            // 'cantidad',
            // 'monto',
            // 'cantidad_cambio',
            // 'monto_cambio',
            // 'estado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<div class="tile-text">
    <a href="javascript:void(0);" id="send_email" class="btn btn-default waves-effect"><i
            class="fa fa-envelope-square"></i> Enviar </a>
</div>
<script type="text/javascript">

    $(document).ready(function () {

        // click send email button
        $('#send_email').click(function (e) {
            var keys = $('#w0').yiiGridView('getSelectedRows');
            console.log('click');

            // con esto mando los proveedores
            if (keys == "") {
                alert("No ha seleccionado proveedores");
            } else {
                var id = '< ?= $this->context->actionParams[ 'id' ]; ?>';
                var url = '< ?= Url::toRoute(['user_job/get_providers']); ?>';
                var data = {
                    id: id,
                    keys: keys,
                    _backendCSRF: '<?= Yii::$app->request->csrfToken ?>'
                };
                $.ajax({
                    url: url,
                    data: data,
                    method: 'POST',
                    cache: false,
                    beforeSend: function () {
                        $('#send_email').html('<i class="fa fa-refresh fa-spin"></i> Enviando');
                    },
                    complete: function (result_s) {
                        console.log(result_s);
                        $('#send_email').html('<i class="fa fa-envelope-square"></i> Enviado ');
                        top.location.href =  '< ?= Url::toRoute(['user_job/view?id='.$id]); ?>';
                    }
                });
            }
        });

    });
</script>