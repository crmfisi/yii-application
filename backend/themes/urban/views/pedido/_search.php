<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PedidoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedido-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tipo_pedido_id') ?>

    <?= $form->field($model, 'interlocutor_comercial_id') ?>

    <?= $form->field($model, 'cantidad_producto') ?>

    <?= $form->field($model, 'descripcion__producto') ?>

    <?php // echo $form->field($model, 'precio_unitario') ?>

    <?php // echo $form->field($model, 'cantidad') ?>

    <?php // echo $form->field($model, 'monto') ?>

    <?php // echo $form->field($model, 'cantidad_cambio') ?>

    <?php // echo $form->field($model, 'monto_cambio') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
