<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Zona */

$this->title = Yii::t('zona', 'Update {modelClass}: ', [
    'modelClass' => 'Zona',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('zona', 'Zonas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('zona', 'Update');
?>
<div class="zona-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
