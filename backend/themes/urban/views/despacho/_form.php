<?php

use yii\helpers\Html; // CABECERA PARA USO DE LA FUNCION HTML:ENCODE
use yii\bootstrap\ActiveForm; // CABECERA PARA USO DE FORMULARIOS EN LA PAGINA
use yii\helpers\ArrayHelper; // CABECERA PARA USO DE ARRAYS A MUCHOS TIPOS DE ESCALA
use backend\models\Operacion;
use backend\models\Pedido;

/* @var $this yii\web\View */
/* @var $model backend\models\Despacho */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-body ">
    <div class="row no-margin">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal bordered-group'],
                'fieldConfig' => [
                    'template' => '{label}<div class="col-sm-10 col-lg-8">{input}{hint}{error}</div>',
                    'labelOptions' => [
                        'class' => 'col-sm-2 control-label'
                    ]
                ]
            ]); ?>

            <div class="form-group">
                <?= $form->field($model, 'id')->textInput() ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'operacion_id')->textInput() ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'pedido_id')->textInput() ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'estado')->textInput() ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'fecha')->textInput() ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('despacho', 'Create') : Yii::t('despacho', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
