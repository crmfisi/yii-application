<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Despacho */

$this->title = Yii::t('despacho', 'Create Despacho');
$this->params['breadcrumbs'][] = ['label' => Yii::t('despacho', 'Despachos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="despacho-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
