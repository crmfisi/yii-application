<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Despacho */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('despacho', 'Despachos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="despacho-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('despacho', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('despacho', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('despacho', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'operacion_id',
            'pedido_id',
            'estado',
            'fecha',
        ],
    ]) ?>

</div>
