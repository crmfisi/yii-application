<?php
/**
 * User Create: Cesar Mamani Dominguez
 * Date: 25/06/2016
 * Time: 11:16 AM
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\Operacion_detalleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="panel-body ">
    <div class="row no-margin">
        <div class="col-lg-12">

            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal bordered-group'],
                
                'fieldConfig' => [
                    'template' => '{label}<div class="col-sm-10 col-lg-8">{input}{hint}{error}</div>',
                    'labelOptions' => [
                        'class' => 'col-sm-2 control-label'
                    ]
                ]
            ]); ?>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Secuencia</label>
                <div class="col-sm-10 col-lg-8">
                    <?= $form->field($model, 'secuencia')->textInput()->label('') ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Precio unitario</label>
                <div class="col-sm-10 col-lg-8">
                    <?= $form->field($model, 'precio_unitario')->textInput()->label('') ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Cantidad cambio</label>
                <div class="col-sm-10 col-lg-8">
                    <?= $form->field($model, 'cantidad_cambio')->textInput()->label('') ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Monto cambio</label>
                <div class="col-sm-10 col-lg-8">
                    <?= $form->field($model, 'monto_cambio')->textInput()->label('') ?>
                </div>
            </div>

<!--            <div class="form-group">-->
<!--                <label class="col-sm-2 control-label" for="attribute_select">Observacion</label>-->
<!--                <div class="col-sm-10 col-lg-8">-->
<!--                    < ?= $form->field($model, 'observacion')->textInput()->label('') ?>-->
<!--                </div>-->
<!--            </div>-->

            <div class="form-group">
                <?= Html::submitButton( 'Guardar cambio' , ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>