<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Operacion_detalle */

$this->title = Yii::t('operacion_detalle', 'Create Operacion Detalle');
$this->params['breadcrumbs'][] = ['label' => Yii::t('operacion_detalle', 'Operacion Detalles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacion-detalle-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
