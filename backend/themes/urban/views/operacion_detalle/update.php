<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Operacion_detalle */

$this->title = Yii::t('operacion_detalle', 'Update {modelClass}: ', [
    'modelClass' => 'Operacion Detalle',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('operacion_detalle', 'Operacion Detalles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('operacion_detalle', 'Update');
?>
<div class="operacion-detalle-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
