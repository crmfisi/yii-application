<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Operacion_detalle */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('operacion_detalle', 'Operacion Detalles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacion-detalle-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('operacion_detalle', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('operacion_detalle', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('operacion_detalle', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'producto_id',
            'producto_id_operacion',
            'operacion_id',
            'zona_id',
            'cantidad',
            'monto',
            'secuencia',
            'estado',
            'fecha_operacion',
        ],
    ]) ?>

</div>
