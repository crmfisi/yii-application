<?php

use yii\helpers\Html; // CABECERA PARA USO DE LA FUNCION HTML:ENCODE
use yii\bootstrap\ActiveForm; // CABECERA PARA USO DE FORMULARIOS EN LA PAGINA
use yii\helpers\ArrayHelper; // CABECERA PARA USO DE ARRAYS A MUCHOS TIPOS DE ESCALA
use backend\models\Producto;
use backend\models\Operacion;
use backend\models\Zona;

/* @var $this yii\web\View */
/* @var $model backend\models\Operacion_detalle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-body ">
    <div class="row no-margin">
        <div class="col-lg-12">

            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal bordered-group'],
                'fieldConfig' => [
                    'template' => '{label}<div class="col-sm-10 col-lg-8">{input}{hint}{error}</div>',
                    'labelOptions' => [
                        'class' => 'col-sm-2 control-label'
                    ]
                ]
            ]); ?>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Producto</label>
                <div class="col-sm-10 col-lg-8">
                    <?php $field = $form->field($model,
                        'producto_id')->dropDownList(ArrayHelper::map(Producto::find()->orderBy('nombre')->all(),
                        'id', 'nombre'), [
                        'class' => 'form-control',
                        'prompt' => Yii::t('app', 'Seleccionar')])->label(Yii::t('app',
                        ''));
                    echo $field;
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Producto Operacion</label>
                <div class="col-sm-10 col-lg-8">
                    <?php $field = $form->field($model,
                        'producto_id_operacion')->dropDownList(ArrayHelper::map(Producto::find()->orderBy('nombre')->all(),
                        'id', 'nombre'), [
                        'class' => 'form-control',
                        'prompt' => Yii::t('app', 'Seleccionar')])->label(Yii::t('app',
                        ''));
                    echo $field;
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Operacion</label>
                <div class="col-sm-10 col-lg-8">
                    <?php $field = $form->field($model,
                        'operacion_id')->dropDownList(ArrayHelper::map(Operacion::find()->orderBy('codigo_operacion')->all(),
                        'id', 'codigo_operacion'), [
                        'class' => 'form-control',
                        'prompt' => Yii::t('app', 'Seleccionar')])->label(Yii::t('app',
                        ''));
                    echo $field;
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="attribute_select">Zona</label>
                <div class="col-sm-10 col-lg-8">
                    <?php $field = $form->field($model,
                        'zona_id')->dropDownList(ArrayHelper::map(Zona::find()->orderBy('nombre')->all(),
                        'id', 'nombre'), [
                        'class' => 'form-control',
                        'prompt' => Yii::t('app', 'Seleccionar')])->label(Yii::t('app',
                        ''));
                    echo $field;
                    ?>
                </div>
            </div>

            <?= $form->field($model, 'cantidad')->textInput() ?>

            <?= $form->field($model, 'monto')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'secuencia')->textInput() ?>

            <?= $form->field($model, 'estado')->textInput() ?>

            <?= $form->field($model, 'fecha_operacion')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('operacion_detalle', 'Create') : Yii::t('operacion_detalle', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
