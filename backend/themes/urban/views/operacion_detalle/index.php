<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\Operacion_detalleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('operacion_detalle', 'Operacion Detalles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacion-detalle-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('operacion_detalle', 'Create Operacion Detalle'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'producto_id',
            'producto_id_operacion',
            'operacion_id',
            'zona_id',
            // 'cantidad',
            // 'monto',
            // 'secuencia',
            // 'estado',
            // 'fecha_operacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
