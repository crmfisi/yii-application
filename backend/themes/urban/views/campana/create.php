<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Campana */

$this->title = Yii::t('campana', 'Create Campana');
$this->params['breadcrumbs'][] = ['label' => Yii::t('campana', 'Campanas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campana-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
