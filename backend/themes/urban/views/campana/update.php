<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Campana */

$this->title = Yii::t('campana', 'Update {modelClass}: ', [
    'modelClass' => 'Campana',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('campana', 'Campanas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('campana', 'Update');
?>
<div class="campana-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
