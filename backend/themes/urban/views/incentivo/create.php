<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Incentivo */

$this->title = Yii::t('incentivo', 'Create Incentivo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('incentivo', 'Incentivos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="incentivo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
