<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Incentivo */

$this->title = Yii::t('incentivo', 'Update {modelClass}: ', [
    'modelClass' => 'Incentivo',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('incentivo', 'Incentivos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('incentivo', 'Update');
?>
<div class="incentivo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
