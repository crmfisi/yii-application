<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IncentivoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('incentivo', 'Incentivos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="incentivo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('incentivo', 'Create Incentivo'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tipo_incentivo_id',
            'puntaje_id',
            'producto_id',
            'nombre',
            // 'estado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
