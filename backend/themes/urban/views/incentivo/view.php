<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Incentivo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('incentivo', 'Incentivos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="incentivo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('incentivo', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('incentivo', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('incentivo', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tipo_incentivo_id',
            'puntaje_id',
            'producto_id',
            'nombre',
            'estado',
        ],
    ]) ?>

</div>
