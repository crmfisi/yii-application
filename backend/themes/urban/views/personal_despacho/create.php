<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Personal_despacho */

$this->title = Yii::t('personal_despacho', 'Create Personal Despacho');
$this->params['breadcrumbs'][] = ['label' => Yii::t('personal_despacho', 'Personal Despachos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-despacho-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
