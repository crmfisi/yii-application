<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\Personal_despachoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('personal_despacho', 'Personal Despachos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-despacho-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('personal_despacho', 'Create Personal Despacho'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'despacho_id',
            'codigo',
            'nombre',
            'apellido',
            // 'email:email',
            // 'telefono',
            // 'estado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
