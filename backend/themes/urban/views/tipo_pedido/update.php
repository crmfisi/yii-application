<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Tipo_pedido */

$this->title = Yii::t('tipo_pedido', 'Update {modelClass}: ', [
    'modelClass' => 'Tipo Pedido',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('tipo_pedido', 'Tipo Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('tipo_pedido', 'Update');
?>
<div class="tipo-pedido-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
