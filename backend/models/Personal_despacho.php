<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "personal_despacho".
 *
 * @property integer $id
 * @property integer $despacho_id
 * @property string $codigo
 * @property string $nombre
 * @property string $apellido
 * @property string $email
 * @property string $telefono
 * @property integer $estado
 *
 * @property Despacho $despacho
 */
class Personal_despacho extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'personal_despacho';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['despacho_id'], 'required'],
            [['despacho_id', 'estado'], 'integer'],
            [['codigo'], 'string', 'max' => 10],
            [['nombre', 'apellido', 'email'], 'string', 'max' => 255],
            [['telefono'], 'string', 'max' => 45],
            [['despacho_id'], 'exist', 'skipOnError' => true, 'targetClass' => Despacho::className(), 'targetAttribute' => ['despacho_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('personal_despacho', 'ID'),
            'despacho_id' => Yii::t('personal_despacho', 'Despacho ID'),
            'codigo' => Yii::t('personal_despacho', 'Codigo'),
            'nombre' => Yii::t('personal_despacho', 'Nombre'),
            'apellido' => Yii::t('personal_despacho', 'Apellido'),
            'email' => Yii::t('personal_despacho', 'Email'),
            'telefono' => Yii::t('personal_despacho', 'Telefono'),
            'estado' => Yii::t('personal_despacho', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDespacho()
    {
        return $this->hasOne(Despacho::className(), ['id' => 'despacho_id']);
    }
}
