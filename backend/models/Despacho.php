<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "despacho".
 *
 * @property integer $id
 * @property integer $operacion_id
 * @property integer $pedido_id
 * @property integer $estado
 * @property string $fecha
 *
 * @property Operacion $operacion
 * @property Pedido $pedido
 * @property Personal_despacho[] $personalDespachos
 * @property Venta[] $ventas
 */
class Despacho extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'despacho';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'operacion_id', 'pedido_id', 'estado'], 'integer'],
            [['fecha'], 'safe'],
            [['operacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Operacion::className(), 'targetAttribute' => ['operacion_id' => 'id']],
            [['pedido_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pedido::className(), 'targetAttribute' => ['pedido_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('despacho', 'ID'),
            'operacion_id' => Yii::t('despacho', 'Operacion ID'),
            'pedido_id' => Yii::t('despacho', 'Pedido ID'),
            'estado' => Yii::t('despacho', 'Estado'),
            'fecha' => Yii::t('despacho', 'Fecha'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacion()
    {
        return $this->hasOne(Operacion::className(), ['id' => 'operacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedido()
    {
        return $this->hasOne(Pedido::className(), ['id' => 'pedido_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalDespachos()
    {
        return $this->hasMany(Personal_despacho::className(), ['despacho_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Venta::className(), ['despacho_id' => 'id']);
    }
}
