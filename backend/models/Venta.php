<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "venta".
 *
 * @property integer $id
 * @property integer $despacho_id
 * @property integer $pedido_id
 *
 * @property Pedido $pedido
 * @property Despacho $despacho
 * @property Venta_detalle[] $ventaDetalles
 */
class Venta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'venta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['despacho_id', 'pedido_id'], 'required'],
            [['despacho_id', 'pedido_id'], 'integer'],
            [['pedido_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pedido::className(), 'targetAttribute' => ['pedido_id' => 'id']],
            [['despacho_id'], 'exist', 'skipOnError' => true, 'targetClass' => Despacho::className(), 'targetAttribute' => ['despacho_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('venta', 'ID'),
            'despacho_id' => Yii::t('venta', 'Despacho ID'),
            'pedido_id' => Yii::t('venta', 'Pedido ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedido()
    {
        return $this->hasOne(Pedido::className(), ['id' => 'pedido_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDespacho()
    {
        return $this->hasOne(Despacho::className(), ['id' => 'despacho_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentaDetalles()
    {
        return $this->hasMany(Venta_detalle::className(), ['venta_id' => 'id']);
    }
}
