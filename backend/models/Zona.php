<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "zona".
 *
 * @property integer $id
 * @property integer $distrito_id
 * @property string $codigo
 * @property string $nombre
 * @property integer $estado
 *
 * @property InterlocutorComercial[] $interlocutorComercials
 * @property OperacionDetalle[] $operacionDetalles
 * @property Distrito $distrito
 */
class Zona extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zona';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['distrito_id'], 'required'],
            [['distrito_id', 'estado'], 'integer'],
            [['codigo'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 255],
            [['distrito_id'], 'exist', 'skipOnError' => true, 'targetClass' => Distrito::className(), 'targetAttribute' => ['distrito_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('zona', 'ID'),
            'distrito_id' => Yii::t('zona', 'Distrito ID'),
            'codigo' => Yii::t('zona', 'Codigo'),
            'nombre' => Yii::t('zona', 'Nombre'),
            'estado' => Yii::t('zona', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterlocutorComercials()
    {
        return $this->hasMany(InterlocutorComercial::className(), ['zona_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacionDetalles()
    {
        return $this->hasMany(OperacionDetalle::className(), ['zona_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrito()
    {
        return $this->hasOne(Distrito::className(), ['id' => 'distrito_id']);
    }
}
