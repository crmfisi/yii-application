<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tipo_incentivo".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $estado
 *
 * @property Incentivo[] $incentivos
 */
class Tipo_incentivo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_incentivo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tipo_incentivo', 'ID'),
            'nombre' => Yii::t('tipo_incentivo', 'Nombre'),
            'estado' => Yii::t('tipo_incentivo', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncentivos()
    {
        return $this->hasMany(Incentivo::className(), ['tipo_incentivo_id' => 'id']);
    }
}
