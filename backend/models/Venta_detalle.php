<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "venta_detalle".
 *
 * @property integer $id
 * @property integer $venta_id
 *
 * @property Venta $venta
 */
class Venta_detalle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'venta_detalle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['venta_id'], 'required'],
            [['venta_id'], 'integer'],
            [['venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['venta_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('venta_detalle', 'ID'),
            'venta_id' => Yii::t('venta_detalle', 'Venta ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVenta()
    {
        return $this->hasOne(Venta::className(), ['id' => 'venta_id']);
    }
}
