<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Operacion_detalle;

/**
 * Operacion_detalleSearch represents the model behind the search form about `backend\models\Operacion_detalle`.
 */
class Operacion_detalleSearch extends Operacion_detalle
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'producto_id', 'producto_id_operacion', 'operacion_id', 'zona_id', 'cantidad', 'secuencia', 'estado'], 'integer'],
            [['monto'], 'number'],
            [['fecha_operacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Operacion_detalle::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'producto_id' => $this->producto_id,
            'producto_id_operacion' => $this->producto_id_operacion,
            'operacion_id' => $this->operacion_id,
            'zona_id' => $this->zona_id,
            'cantidad' => $this->cantidad,
            'monto' => $this->monto,
            'secuencia' => $this->secuencia,
            'estado' => $this->estado,
            'fecha_operacion' => $this->fecha_operacion,
        ]);

        return $dataProvider;
    }
}
