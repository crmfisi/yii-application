<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "entrega_premios".
 *
 * @property integer $id
 * @property integer $producto_id
 *
 * @property Producto $producto
 * @property Ganadoras[] $ganadoras
 */
class Entrega_premios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entrega_premios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producto_id'], 'required'],
            [['producto_id'], 'integer'],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('entrega_premios', 'ID'),
            'producto_id' => Yii::t('entrega_premios', 'Producto ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGanadoras()
    {
        return $this->hasMany(Ganadoras::className(), ['entrega_premios_id' => 'id']);
    }
}
