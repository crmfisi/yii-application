<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "operacion_detalle".
 *
 * @property integer $id
 * @property integer $producto_id
 * @property integer $producto_id_operacion
 * @property integer $operacion_id
 * @property integer $zona_id
 * @property integer $cantidad
 * @property string $monto
 * @property integer $secuencia
 * @property integer $estado
 * @property string $fecha_operacion
 *
 * @property Operacion $operacion
 * @property Producto $producto
 * @property Producto $productoIdOperacion
 * @property Zona $zona
 */
class Operacion_detalle extends \yii\db\ActiveRecord
{
    // public $secuencia;
    public $precio_unitario;
    public $cantidad_cambio;
    public $monto_cambio;
    public $observacion;
    
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operacion_detalle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producto_id', 'operacion_id', 'zona_id'], 'required'],
            [['producto_id', 'producto_id_operacion', 'operacion_id', 'zona_id', 'cantidad', 'secuencia', 'estado'], 'integer'],
            [['monto'], 'number'],
            [['fecha_operacion'], 'safe'],
            [['operacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Operacion::className(), 'targetAttribute' => ['operacion_id' => 'id']],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
            [['producto_id_operacion'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id_operacion' => 'id']],
            [['zona_id'], 'exist', 'skipOnError' => true, 'targetClass' => Zona::className(), 'targetAttribute' => ['zona_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('operacion_detalle', 'ID'),
            'producto_id' => Yii::t('operacion_detalle', 'Producto ID'),
            'producto_id_operacion' => Yii::t('operacion_detalle', 'Producto Id Operacion'),
            'operacion_id' => Yii::t('operacion_detalle', 'Operacion ID'),
            'zona_id' => Yii::t('operacion_detalle', 'Zona ID'),
            'cantidad' => Yii::t('operacion_detalle', 'Cantidad'),
            'monto' => Yii::t('operacion_detalle', 'Monto'),
            'secuencia' => Yii::t('operacion_detalle', 'Secuencia'),
            'estado' => Yii::t('operacion_detalle', 'Estado'),
            'fecha_operacion' => Yii::t('operacion_detalle', 'Fecha Operacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacion()
    {
        return $this->hasOne(Operacion::className(), ['id' => 'operacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductoIdOperacion()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id_operacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZona()
    {
        return $this->hasOne(Zona::className(), ['id' => 'zona_id']);
    }
}
