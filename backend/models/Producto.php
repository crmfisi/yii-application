<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property integer $id
 * @property integer $familia_id
 * @property string $codigo
 * @property string $nombre
 * @property integer $unidad
 * @property string $precio
 * @property string $precio_vta
 * @property integer $descuento
 * @property integer $estado
 *
 * @property CatalogoProducto[] $catalogoProductos
 * @property EntregaPremios[] $entregaPremios
 * @property Incentivo[] $incentivos
 * @property OperacionDetalle[] $operacionDetalles
 * @property OperacionDetalle[] $operacionDetalles0
 * @property Familia $familia
 * @property Stock[] $stocks
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['familia_id'], 'required'],
            [['familia_id', 'unidad', 'descuento', 'estado'], 'integer'],
            [['precio', 'precio_vta'], 'number'],
            [['codigo'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 255],
            [['familia_id'], 'exist', 'skipOnError' => true, 'targetClass' => Familia::className(), 'targetAttribute' => ['familia_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('producto', 'ID'),
            'familia_id' => Yii::t('producto', 'Familia ID'),
            'codigo' => Yii::t('producto', 'Codigo'),
            'nombre' => Yii::t('producto', 'Nombre'),
            'unidad' => Yii::t('producto', 'Unidad'),
            'precio' => Yii::t('producto', 'Precio'),
            'precio_vta' => Yii::t('producto', 'Precio Vta'),
            'descuento' => Yii::t('producto', 'Descuento'),
            'estado' => Yii::t('producto', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogoProductos()
    {
        return $this->hasMany(CatalogoProducto::className(), ['producto_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntregaPremios()
    {
        return $this->hasMany(EntregaPremios::className(), ['producto_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncentivos()
    {
        return $this->hasMany(Incentivo::className(), ['producto_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacionDetalles()
    {
        return $this->hasMany(OperacionDetalle::className(), ['producto_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacionDetalles0()
    {
        return $this->hasMany(OperacionDetalle::className(), ['producto_id_operacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilia()
    {
        return $this->hasOne(Familia::className(), ['id' => 'familia_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStocks()
    {
        return $this->hasMany(Stock::className(), ['producto_id' => 'id']);
    }
}
