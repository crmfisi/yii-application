<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pedido".
 *
 * @property integer $id
 * @property integer $tipo_pedido_id
 * @property integer $interlocutor_comercial_id
 * @property integer $cantidad_producto
 * @property string $descripcion__producto
 * @property string $precio_unitario
 * @property integer $cantidad
 * @property string $monto
 * @property integer $cantidad_cambio
 * @property string $monto_cambio
 * @property integer $estado
 *
 * @property Despacho[] $despachos
 * @property Operacion[] $operacions
 * @property Interlocutor_comercial $interlocutorComercial
 * @property Tipo_pedido $tipoPedido
 * @property Pedido_detalle[] $pedidoDetalles
 * @property Venta[] $ventas
 */
class Pedido extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pedido';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_pedido_id', 'interlocutor_comercial_id'], 'required'],
            [['tipo_pedido_id', 'interlocutor_comercial_id', 'cantidad_producto', 'cantidad', 'cantidad_cambio', 'estado'], 'integer'],
            [['precio_unitario', 'monto', 'monto_cambio'], 'number'],
            [['descripcion__producto'], 'string', 'max' => 255],
            [['interlocutor_comercial_id'], 'exist', 'skipOnError' => true, 'targetClass' => Interlocutor_comercial::className(), 'targetAttribute' => ['interlocutor_comercial_id' => 'id']],
            [['tipo_pedido_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo_pedido::className(), 'targetAttribute' => ['tipo_pedido_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_pedido_id' => 'Tipo Pedido ID',
            'interlocutor_comercial_id' => 'Interlocutor Comercial ID',
            'cantidad_producto' => 'Cantidad Producto',
            'descripcion__producto' => 'Descripcion  Producto',
            'precio_unitario' => 'Precio Unitario',
            'cantidad' => 'Cantidad',
            'monto' => 'Monto',
            'cantidad_cambio' => 'Cantidad Cambio',
            'monto_cambio' => 'Monto Cambio',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDespachos()
    {
        return $this->hasMany(Despacho::className(), ['pedido_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacions()
    {
        return $this->hasMany(Operacion::className(), ['pedido_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterlocutorComercial()
    {
        return $this->hasOne(Interlocutor_comercial::className(), ['id' => 'interlocutor_comercial_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoPedido()
    {
        return $this->hasOne(Tipo_pedido::className(), ['id' => 'tipo_pedido_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoDetalles()
    {
        return $this->hasMany(Pedido_detalle::className(), ['pedido_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Venta::className(), ['pedido_id' => 'id']);
    }
}
