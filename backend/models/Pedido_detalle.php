<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pedido_detalle".
 *
 * @property integer $id
 * @property integer $producto_id
 * @property integer $pedido_id
 *
 * @property Producto $producto
 * @property Pedido $pedido
 */
class Pedido_detalle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pedido_detalle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producto_id', 'pedido_id'], 'required'],
            [['producto_id', 'pedido_id'], 'integer'],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
            [['pedido_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pedido::className(), 'targetAttribute' => ['pedido_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pedido_detalle', 'ID'),
            'producto_id' => Yii::t('pedido_detalle', 'Producto ID'),
            'pedido_id' => Yii::t('pedido_detalle', 'Pedido ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedido()
    {
        return $this->hasOne(Pedido::className(), ['id' => 'pedido_id']);
    }
}
