<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "incentivo".
 *
 * @property integer $id
 * @property integer $tipo_incentivo_id
 * @property integer $puntaje_id
 * @property integer $producto_id
 * @property string $nombre
 * @property integer $estado
 *
 * @property CatalogoProducto[] $catalogoProductos
 * @property Producto $producto
 * @property Puntaje $puntaje
 * @property Tipo_incentivo $tipoIncentivo
 */
class Incentivo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'incentivo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_incentivo_id', 'puntaje_id', 'producto_id'], 'required'],
            [['tipo_incentivo_id', 'puntaje_id', 'producto_id', 'estado'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
            [['puntaje_id'], 'exist', 'skipOnError' => true, 'targetClass' => Puntaje::className(), 'targetAttribute' => ['puntaje_id' => 'id']],
            [['tipo_incentivo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo_incentivo::className(), 'targetAttribute' => ['tipo_incentivo_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('incentivo', 'ID'),
            'tipo_incentivo_id' => Yii::t('incentivo', 'Tipo Incentivo ID'),
            'puntaje_id' => Yii::t('incentivo', 'Puntaje ID'),
            'producto_id' => Yii::t('incentivo', 'Producto ID'),
            'nombre' => Yii::t('incentivo', 'Nombre'),
            'estado' => Yii::t('incentivo', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogoProductos()
    {
        return $this->hasMany(Catalogo_producto::className(), ['incentivo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPuntaje()
    {
        return $this->hasOne(Puntaje::className(), ['id' => 'puntaje_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoIncentivo()
    {
        return $this->hasOne(Tipo_incentivo::className(), ['id' => 'tipo_incentivo_id']);
    }
}
