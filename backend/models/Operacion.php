<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "operacion".
 *
 * @property integer $id
 * @property integer $tipo_operacion_id
 * @property integer $pedido_id
 * @property string $codigo_operacion
 * @property string $monto_operacion
 * @property integer $estado
 * @property string $observacion
 * @property string $fecha_operacion
 *
 * @property Pedido $pedido
 * @property Tipo_operacion $tipoOperacion
 * @property Operacion_detalle[] $operacionDetalles
 */
class Operacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_operacion_id', 'pedido_id', 'observacion'], 'required'],
            [['tipo_operacion_id', 'pedido_id', 'estado'], 'integer'],
            [['monto_operacion'], 'number'],
            [['fecha_operacion'], 'safe'],
            [['codigo_operacion'], 'string', 'max' => 255],
            [['observacion'], 'string', 'max' => 45],
            [['pedido_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pedido::className(), 'targetAttribute' => ['pedido_id' => 'id']],
            [['tipo_operacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo_operacion::className(), 'targetAttribute' => ['tipo_operacion_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('operacion', 'ID'),
            'tipo_operacion_id' => Yii::t('operacion', 'Tipo Operacion ID'),
            'pedido_id' => Yii::t('operacion', 'Pedido ID'),
            'codigo_operacion' => Yii::t('operacion', 'Codigo Operacion'),
            'monto_operacion' => Yii::t('operacion', 'Monto Operacion'),
            'estado' => Yii::t('operacion', 'Estado'),
            'observacion' => Yii::t('operacion', 'Observacion'),
            'fecha_operacion' => Yii::t('operacion', 'Fecha Operacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedido()
    {
        return $this->hasOne(Pedido::className(), ['id' => 'pedido_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoOperacion()
    {
        return $this->hasOne(Tipo_operacion::className(), ['id' => 'tipo_operacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacionDetalles()
    {
        return $this->hasMany(Operacion_detalle::className(), ['operacion_id' => 'id']);
    }
}
