<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ganadoras".
 *
 * @property integer $id
 * @property integer $entrega_premios_id
 * @property integer $interlocutor_comercial_id
 *
 * @property EntregaPremios $entregaPremios
 * @property InterlocutorComercial $interlocutorComercial
 */
class Ganadoras extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ganadoras';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'entrega_premios_id', 'interlocutor_comercial_id'], 'required'],
            [['id', 'entrega_premios_id', 'interlocutor_comercial_id'], 'integer'],
            [['entrega_premios_id'], 'exist', 'skipOnError' => true, 'targetClass' => EntregaPremios::className(), 'targetAttribute' => ['entrega_premios_id' => 'id']],
            [['interlocutor_comercial_id'], 'exist', 'skipOnError' => true, 'targetClass' => InterlocutorComercial::className(), 'targetAttribute' => ['interlocutor_comercial_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('ganadoras', 'ID'),
            'entrega_premios_id' => Yii::t('ganadoras', 'Entrega Premios ID'),
            'interlocutor_comercial_id' => Yii::t('ganadoras', 'Interlocutor Comercial ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntregaPremios()
    {
        return $this->hasOne(EntregaPremios::className(), ['id' => 'entrega_premios_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterlocutorComercial()
    {
        return $this->hasOne(InterlocutorComercial::className(), ['id' => 'interlocutor_comercial_id']);
    }
}
