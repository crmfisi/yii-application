<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "distrito".
 *
 * @property integer $id
 * @property string $codigo
 * @property string $nombre
 * @property string $latitud
 * @property string $longitud
 * @property integer $estado
 *
 * @property Zona[] $zonas
 */
class Distrito extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distrito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado'], 'integer'],
            [['codigo'], 'string', 'max' => 10],
            [['nombre', 'latitud', 'longitud'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('distrito', 'ID'),
            'codigo' => Yii::t('distrito', 'Codigo'),
            'nombre' => Yii::t('distrito', 'Nombre'),
            'latitud' => Yii::t('distrito', 'Latitud'),
            'longitud' => Yii::t('distrito', 'Longitud'),
            'estado' => Yii::t('distrito', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZonas()
    {
        return $this->hasMany(Zona::className(), ['distrito_id' => 'id']);
    }
}
