<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tipo_pedido".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $estado
 *
 * @property Pedido[] $pedidos
 */
class Tipo_pedido extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_pedido';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tipo_pedido', 'ID'),
            'nombre' => Yii::t('tipo_pedido', 'Nombre'),
            'estado' => Yii::t('tipo_pedido', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedido::className(), ['tipo_pedido_id' => 'id']);
    }
}
