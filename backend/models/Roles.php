<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "roles".
 *
 * @property integer $id
 * @property string $codigo
 * @property string $nombre
 * @property integer $estado
 *
 * @property InterlocutorComercial[] $interlocutorComercials
 */
class Roles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'roles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado'], 'integer'],
            [['codigo'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('roles', 'ID'),
            'codigo' => Yii::t('roles', 'Codigo'),
            'nombre' => Yii::t('roles', 'Nombre'),
            'estado' => Yii::t('roles', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterlocutorComercials()
    {
        return $this->hasMany(InterlocutorComercial::className(), ['roles_id' => 'id']);
    }
}
