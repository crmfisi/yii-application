<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "interlocutor_comercial".
 *
 * @property integer $id
 * @property integer $zona_id
 * @property integer $roles_id
 * @property string $codigo
 * @property string $nombre
 * @property string $apellido
 * @property string $email
 * @property string $telefono
 * @property integer $estado
 *
 * @property Ganadoras[] $ganadoras
 * @property Roles $roles
 * @property Zona $zona
 * @property Pedido[] $pedidos
 * @property Puntaje[] $puntajes
 * @property Relacion[] $relacions
 * @property Relacion[] $relacions0
 */
class Interlocutor_comercial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'interlocutor_comercial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['zona_id', 'roles_id'], 'required'],
            [['zona_id', 'roles_id', 'estado'], 'integer'],
            [['codigo'], 'string', 'max' => 10],
            [['nombre', 'apellido', 'email'], 'string', 'max' => 255],
            [['telefono'], 'string', 'max' => 45],
            [['roles_id'], 'exist', 'skipOnError' => true, 'targetClass' => Roles::className(), 'targetAttribute' => ['roles_id' => 'id']],
            [['zona_id'], 'exist', 'skipOnError' => true, 'targetClass' => Zona::className(), 'targetAttribute' => ['zona_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('interlocutor_comercial', 'ID'),
            'zona_id' => Yii::t('interlocutor_comercial', 'Zona ID'),
            'roles_id' => Yii::t('interlocutor_comercial', 'Roles ID'),
            'codigo' => Yii::t('interlocutor_comercial', 'Codigo'),
            'nombre' => Yii::t('interlocutor_comercial', 'Nombre'),
            'apellido' => Yii::t('interlocutor_comercial', 'Apellido'),
            'email' => Yii::t('interlocutor_comercial', 'Email'),
            'telefono' => Yii::t('interlocutor_comercial', 'Telefono'),
            'estado' => Yii::t('interlocutor_comercial', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGanadoras()
    {
        return $this->hasMany(Ganadoras::className(), ['interlocutor_comercial_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasOne(Roles::className(), ['id' => 'roles_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZona()
    {
        return $this->hasOne(Zona::className(), ['id' => 'zona_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedido::className(), ['interlocutor_comercial_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPuntajes()
    {
        return $this->hasMany(Puntaje::className(), ['interlocutor_comercial_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelacions()
    {
        return $this->hasMany(Relacion::className(), ['interlocutor_comercial_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelacions0()
    {
        return $this->hasMany(Relacion::className(), ['interlocutor_comercial_id1' => 'id']);
    }
}
