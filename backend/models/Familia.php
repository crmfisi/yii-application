<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "familia".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $estado
 *
 * @property Producto[] $productos
 */
class Familia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'familia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'estado'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('familia', 'ID'),
            'nombre' => Yii::t('familia', 'Nombre'),
            'estado' => Yii::t('familia', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Producto::className(), ['familia_id' => 'id']);
    }
}
