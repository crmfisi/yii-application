<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Pedido;

/**
 * PedidoSearch represents the model behind the search form about `backend\models\Pedido`.
 */
class PedidoSearch extends Pedido
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tipo_pedido_id', 'interlocutor_comercial_id', 'cantidad_producto', 'cantidad', 'cantidad_cambio', 'estado'], 'integer'],
            [['descripcion__producto'], 'safe'],
            [['precio_unitario', 'monto', 'monto_cambio'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pedido::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tipo_pedido_id' => $this->tipo_pedido_id,
            'interlocutor_comercial_id' => $this->interlocutor_comercial_id,
            'cantidad_producto' => $this->cantidad_producto,
            'precio_unitario' => $this->precio_unitario,
            'cantidad' => $this->cantidad,
            'monto' => $this->monto,
            'cantidad_cambio' => $this->cantidad_cambio,
            'monto_cambio' => $this->monto_cambio,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'descripcion__producto', $this->descripcion__producto]);

        return $dataProvider;
    }
}
