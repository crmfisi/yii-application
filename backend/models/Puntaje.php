<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "puntaje".
 *
 * @property integer $id
 * @property integer $interlocutor_comercial_id
 * @property integer $puntaje
 * @property integer $estado
 * @property string $fecha
 *
 * @property Incentivo[] $incentivos
 * @property Interlocutor_comercial $interlocutorComercial
 */
class Puntaje extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'puntaje';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['interlocutor_comercial_id'], 'required'],
            [['interlocutor_comercial_id', 'puntaje', 'estado'], 'integer'],
            [['fecha'], 'safe'],
            [['interlocutor_comercial_id'], 'exist', 'skipOnError' => true, 'targetClass' => Interlocutor_comercial::className(), 'targetAttribute' => ['interlocutor_comercial_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('puntaje', 'ID'),
            'interlocutor_comercial_id' => Yii::t('puntaje', 'Interlocutor Comercial ID'),
            'puntaje' => Yii::t('puntaje', 'Puntaje'),
            'estado' => Yii::t('puntaje', 'Estado'),
            'fecha' => Yii::t('puntaje', 'Fecha'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncentivos()
    {
        return $this->hasMany(Incentivo::className(), ['puntaje_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterlocutorComercial()
    {
        return $this->hasOne(Interlocutor_comercial::className(), ['id' => 'interlocutor_comercial_id']);
    }
}
