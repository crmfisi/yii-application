<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "catalogo".
 *
 * @property integer $id
 * @property string $descripcion
 * @property integer $mes
 * @property integer $anio
 * @property integer $estado
 * @property string $codigo
 *
 * @property CatalogoProducto[] $catalogoProductos
 */
class Catalogo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalogo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mes', 'anio', 'estado'], 'integer'],
            [['descripcion'], 'string', 'max' => 255],
            [['codigo'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('catalogo', 'ID'),
            'descripcion' => Yii::t('catalogo', 'Descripcion'),
            'mes' => Yii::t('catalogo', 'Mes'),
            'anio' => Yii::t('catalogo', 'Anio'),
            'estado' => Yii::t('catalogo', 'Estado'),
            'codigo' => Yii::t('catalogo', 'Codigo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogoProductos()
    {
        return $this->hasMany(Catalogo_producto::className(), ['catalogo_id' => 'id']);
    }
}
