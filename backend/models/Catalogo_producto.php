<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "catalogo_producto".
 *
 * @property integer $id
 * @property integer $catalogo_id
 * @property integer $campana_id
 * @property integer $producto_id
 * @property string $descripcion
 * @property integer $estado
 *
 * @property Catalogo $catalogo
 * @property Incentivo $incentivo
 * @property Producto $producto
 * @property Campana $campana
 */
class Catalogo_producto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalogo_producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalogo_id', 'campana_id', 'producto_id', ], 'required'],
            [['catalogo_id', 'campana_id', 'producto_id', 'estado'], 'integer'],
            [['descripcion'], 'string', 'max' => 255],
            [['catalogo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Catalogo::className(), 'targetAttribute' => ['catalogo_id' => 'id']],
            [['incentivo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Incentivo::className(), 'targetAttribute' => ['incentivo_id' => 'id']],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
            [['campana_id'], 'exist', 'skipOnError' => true, 'targetClass' => Campana::className(), 'targetAttribute' => ['campana_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('catalogo_producto', 'ID'),
            'catalogo_id' => Yii::t('catalogo_producto', 'Catalogo ID'),
            'campana_id' => Yii::t('catalogo_producto', 'Campana ID'),
            'producto_id' => Yii::t('catalogo_producto', 'Producto ID'),
            'incentivo_id' => Yii::t('catalogo_producto', 'Incentivo ID'),
            'descripcion' => Yii::t('catalogo_producto', 'Descripcion'),
            'estado' => Yii::t('catalogo_producto', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogo()
    {
        return $this->hasOne(Catalogo::className(), ['id' => 'catalogo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncentivo()
    {
        return $this->hasOne(Incentivo::className(), ['id' => 'incentivo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampana()
    {
        return $this->hasOne(Campana::className(), ['id' => 'campana_id']);
    }
}
