<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "campana".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $estado
 *
 * @property CatalogoProducto[] $catalogoProductos
 */
class Campana extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campana';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('campana', 'ID'),
            'nombre' => Yii::t('campana', 'Nombre'),
            'estado' => Yii::t('campana', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogoProductos()
    {
        return $this->hasMany(CatalogoProducto::className(), ['campana_id' => 'id']);
    }
}
