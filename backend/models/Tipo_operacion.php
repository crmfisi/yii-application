<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tipo_operacion".
 *
 * @property integer $id
 * @property string $detalle
 * @property integer $estado
 *
 * @property Operacion[] $operacions
 */
class Tipo_operacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_operacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado'], 'integer'],
            [['detalle'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tipo_operacion', 'ID'),
            'detalle' => Yii::t('tipo_operacion', 'Detalle'),
            'estado' => Yii::t('tipo_operacion', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacions()
    {
        return $this->hasMany(Operacion::className(), ['tipo_operacion_id' => 'id']);
    }
}
