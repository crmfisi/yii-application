<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'es-LA',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'es-LA',
                ],
            ],
        ],
    ],
    'modules' => [
        // This module is a full characters of gii of mongodb (mongo_gii).

        'debug' => [
            'class' => 'yii\\debug\\Module',
        ],
    ],
];
